FROM registry.gitlab.com/nickptails/neovim

RUN apk add --no-cache \
        bat \
        fd \
        fzf \
        netcat-openbsd \
        ripgrep
